# Copyright (C) 2021 Taavi Väänänen <hi@taavi.wtf>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from dataclasses import dataclass
from typing import Dict, List, Tuple

import yaml


@dataclass
class ChannelConfig:
    topic_separator: str
    topic_section: str
    replica_channels: List[str]


@dataclass
class Config:
    server: Tuple[str, int, bool]
    nickname: str
    username: str
    realname: str
    password: str

    channels: Dict[str, ChannelConfig]


def load(filepath: str):
    with open(filepath) as file:
        config_yaml = yaml.safe_load(file.read())

    nickname = config_yaml["nickname"]

    server = config_yaml["server"]
    hostname, port_s = server.split(":", 1)
    tls = False

    if port_s.startswith("+"):
        tls = True
        port_s = port_s.lstrip("+")
    port = int(port_s)

    channels = {}
    for channel_name, channel_config in config_yaml["channels"].items():
        channels[channel_name] = ChannelConfig(
            channel_config.get("topic_separator", "|"),
            channel_config.get("topic_section", "Status: "),
            channel_config.get("replica_channels", []),
        )

    return Config(
        (hostname, port, tls),
        nickname,
        config_yaml.get("username", nickname),
        config_yaml.get("realname", nickname),
        config_yaml["password"],
        channels,
    )
