# Copyright (C) 2021 Taavi Väänänen <hi@taavi.wtf>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from ircrobots import Bot as BaseBot
from ircrobots import Server as BaseServer
from irctokens import Line, build

from topicmirrorbot.config import Config


class Server(BaseServer):
    def __init__(self, bot: BaseBot, name: str, config: Config):
        super().__init__(bot, name)
        self._config = config

    async def line_read(self, line: Line):
        if line.command == "TOPIC":
            channel, topic = line.params

            if channel not in self._config.channels:
                return
            channel_config = self._config.channels[channel]
            separated = topic.split(channel_config.topic_separator)

            new_data = None
            for section in separated:
                if section.startswith(
                    channel_config.topic_section
                ) or section.startswith(f" {channel_config.topic_section}"):
                    new_data = section

            if not new_data:
                return

            for replica in channel_config.replica_channels:
                if not self.has_channel(replica):
                    continue
                replica_channel = self.get_channel(replica)
                new_replica_topic = ""

                for section in replica_channel.topic.split(
                    channel_config.topic_separator
                ):
                    if section.startswith(
                        channel_config.topic_section
                    ) or section.startswith(f" {channel_config.topic_section}"):
                        new_replica_topic += new_data
                    else:
                        new_replica_topic += section
                    new_replica_topic += channel_config.topic_separator

                new_replica_topic = new_replica_topic[
                    : -len(channel_config.topic_separator)
                ]

                print(f"Setting topic of {replica} to {new_replica_topic}")

                if "o" in replica_channel.users.get(self.nickname_lower).modes:
                    await self.send(build("TOPIC", [channel, new_replica_topic]))
                else:
                    await self.send(
                        build(
                            "PRIVMSG",
                            ["ChanServ", f"TOPIC {replica} {new_replica_topic}"],
                        )
                    )

    def line_preread(self, line: Line):
        print(f"< {line.format()}")

    def line_presend(self, line: Line):
        print(f"> {line.format()}")


class Bot(BaseBot):
    def __init__(self, config: Config):
        super().__init__()
        self._config = config

    def create_server(self, name: str):
        return Server(self, name, self._config)
