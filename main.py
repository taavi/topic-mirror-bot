# Copyright (C) 2021 Taavi Väänänen <hi@taavi.wtf>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import asyncio
from argparse import ArgumentParser

from ircrobots import ConnectionParams, SASLUserPass

from topicmirrorbot.bot import Bot
from topicmirrorbot.config import Config
from topicmirrorbot.config import load as config_load


async def start(config: Config):
    bot = Bot(config)

    host, port, tls = config.server

    channels = []
    for channel_name, channel in config.channels.items():
        channels.extend([channel_name] + channel.replica_channels)

    print(channels)

    params = ConnectionParams(
        config.nickname,
        host,
        port,
        tls,
        username=config.username,
        realname=config.realname,
        password=config.password,
        sasl=SASLUserPass(config.username, config.password),
        autojoin=channels,
    )

    await bot.add_server(host, params)
    await bot.run()


def main():
    parser = ArgumentParser()
    parser.add_argument("config")
    args = parser.parse_args()

    config = config_load(args.config)
    asyncio.run(start(config))


if __name__ == "__main__":
    main()
